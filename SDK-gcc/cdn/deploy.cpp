#include "deploy.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <queue>
#include <map>
#include <string.h>
#include <cmath>
#include <time.h>
#include <sys/timeb.h>
#include <algorithm>

using namespace std;

// (20 + 1) * 2
#define MAX_LINK_PER_NODE 500

#define INF 1000000
#define NUM_EDGES 0
#define NUM_NODES 0
#define SPFA_N 1000
#define MAX_NODE_PER_LINK 1000
#define MAX_PATH_NUM 50000

#define MAX_OUTPUT_STRING_LENGTH 50000

//初始温度
#define T0 100
//最低温度
#define T_MIN 1
//内层迭代次数
#define INTRA_ITER_NUM 50
//外层迭代终止条件：开销改进下限
#define MAX_CHANGE 1

//不考虑邻域算法中增加server策略的最小server占比
#define MIN_ADD_SERVER 0.25

#define MAX_RUNTIME 89000

#define MAX_REDUCE_NUM 6

#define MAX_REDUCE_UNCHANGE 3

//数据结构定义
struct Edge{
    int from, to, cap, cost, flow;
    Edge(int f, int t, int ca, int co, int fl):from(f), to(t), cap(ca), cost(co), flow(fl){};
};

//结构体：服务器部署位置
struct ServerDeploy{
    int* servers;
    int num;
    int cost;
    ServerDeploy(int* s, int n, int c):servers(s), num(n), cost(c){};
};

//网络节点数量
int numNodes;

//网络链路数量,指的是原网络中的无向边数量
int numLinks;

//消费节点数量
int numClients;

//视频服务器部署成本
int costOfVideoNode;

//数组指针，指向消费节点index数组
int* clients;

//记录链接点与消费节点的映射
map<int, int> client_consumer_map;

//数组指针，指向消费节点需求数组
int* clientReqs;

//网络消费节点总需求
int totalReqs;

//数组指针，指向存有图中边的数组，该图为有向图，并且预留添加“反向边”的空间
//具体结构如下：
//0 1   2   3   4   5   6   7   8   9.....
//边 预留  边   预留  边   预留  边   预留  边   预留
//实际边与添加的反向边相邻，并且实边在前，预留边在后
//这样做是为了算最小费用最大流是可以直接memcpy
Edge* edges;

//邻接链表，记录从节点出去的边
//规定每条链的第一个元素记录该链的长度
int (*graph)[MAX_LINK_PER_NODE];

//保存当前最优解
int* best_srcs;
int best_numSrcs;
Edge* best_edges;
int best_totalCost;

//一次减少节点的个数
int reduceNum = MAX_REDUCE_NUM;
//一直减不动从而减少减少数量的最多次数
int reduceUnchange = 0;

//记录程序启动的时间
long int start_time;

struct timeb rawtime;
inline long int getMs(){
    ftime(&rawtime);
    long int s = rawtime.time;
    int ms = rawtime.millitm;
    return s * 1000 + ms;
}


struct Node{
    int request;
    int index;
    Node(int d, int i):request(d), index(i){};
    bool operator < (const Node &a) const {
        //最大值优先
        return request > a.request;
    }
};


void initData(char* topo[MAX_EDGE_NUM], int line_num){

    //处理第一行数据，读取网络节点数量，网络链路数量， 消费节点数量
    sscanf(topo[0], "%d %d %d\r\n", &numNodes, &numLinks, &numClients);
    /*********读取网络节点数量，网络链路数量， 消费节点数量......完毕**********/

    //处理第三行数据，读取视频节点的开销
    sscanf(topo[2], "%d\r\n", &costOfVideoNode);
    /********************读取视频节点开销完毕********************/

    //从第5行开始，处理链路信息
    int k = 4;

    //创建边数组
    edges = (Edge* )malloc(numLinks * sizeof(Edge) * 2 * 2);

    //创建图的邻接链表
    graph = (int (*)[MAX_LINK_PER_NODE])malloc(sizeof(int) * numNodes * MAX_LINK_PER_NODE);
    memset(graph, 0, sizeof(int) * numNodes * MAX_LINK_PER_NODE);

    //读取一行（一条无向链路）时记录用变量
    int src;
    int dst;
    int cap;
    int cost;

    //逐行读取链路
    int i = 0;
    for(; k < line_num; k++){
        if(0 == strcmp(topo[k], "\r\n"))
            break;
        //读取每行的内容
        sscanf(topo[k], "%d %d %d %d\r\n", &src, &dst, &cap, &cost);
        //printf("%d %d %d %d\r\n", src, dst, cap, cost);

        //一条无向边分解成两条有向边，并添加“反向边”

        //正向边及其“反向边”
        edges[i] = Edge(src, dst, cap, cost, 0);
        edges[i + 1] = Edge(dst, src, 0, -cost, 0);

        graph[src][++graph[src][0]] = i;
        graph[dst][++graph[dst][0]] = i + 1;

        i = i + 2;

        //反向边及其“反向边”
        edges[i] = Edge(dst, src, cap, cost, 0);
        edges[i + 1] = Edge(src, dst, 0, -cost, 0);

        graph[dst][++graph[dst][0]] = i;
        graph[src][++graph[src][0]] = i + 1;

        i = i + 2;
    }

    /*****************链路处理完毕***********************************/

    //处理消费节点及其需求
    //创建消费节点数组
    clients = (int* )malloc(sizeof(int) * numClients);
    //创建消费节点需求数组
    clientReqs = (int* )malloc(sizeof(int) * numClients);

    //读取一行（一个消费节点）时记录用变量
    int index;
    int client;
    int clientReq;

    //逐行读取消费节点信息，填充clients、clientReqs数组,计算总流量需求
    totalReqs = 0;
    client_consumer_map.clear();
    for(k++; k < line_num; k++){
        sscanf(topo[k], "%d %d %d\r\n", &index, &client, &clientReq);
        clients[index] = client;
        clientReqs[index] = clientReq;
        totalReqs += clientReq;

        client_consumer_map[client] = index;
        //printf("%d %d %d\n", index, clients[index], clientReqs[index]);
    }
    /*****************消费节点处理完毕******************************/

    //构建初始最优解,初始最优解为“直连”
    best_numSrcs = numClients;
    best_srcs = (int* )malloc(sizeof(int) * best_numSrcs);
    memcpy(best_srcs, clients, sizeof(int) * numClients);
    best_edges = (Edge* )malloc(numLinks * sizeof(Edge) * 2 * 2);
    memcpy(best_edges, edges, numLinks * sizeof(Edge) * 2 * 2);

    best_totalCost = best_numSrcs * costOfVideoNode;
    /*****************初始最优解构建完毕******************************/



}

//定义SPFA需要使用到的变量,定义足够的空间，免得在每次SPFA时需要重新分配空间
int SPFA_d[SPFA_N]; //记录到每个点的最短路径
int SPFA_inq[SPFA_N];   //记录点是否在队列中
int SPFA_p[SPFA_N]; //查找过程中记录节点的前向路径
int SPFA_a[SPFA_N]; //记录路径的最大可用带宽


bool SPFA(int _src, int _dst, int (*_graph)[MAX_LINK_PER_NODE], Edge* _edges, int _numNodes, int _numDirectLinks, int& _flow, int& _cost){
    for (int i = 0; i < _numNodes; i ++)
    {
        SPFA_d[i] = INF;
    }

    memset(SPFA_inq, 0, sizeof(SPFA_inq));
    SPFA_d[_src] = 0;
    SPFA_inq[_src] = 1;
    SPFA_p[_src] = 0;
    SPFA_a[_src] = INF;

    queue<int> nodeQueue;

    nodeQueue.push(_src);

    while (!nodeQueue.empty())
    {
        int _u = nodeQueue.front();
        nodeQueue.pop();
        SPFA_inq[_u] --;
        for(int i = 1; i <= _graph[_u][0]; i++){
            Edge& _e = _edges[_graph[_u][i]];
            if (_e. cap > _e.flow && SPFA_d[_e.to] > SPFA_d[_u] + _e.cost)
            {
                SPFA_d[_e.to] = SPFA_d[_u] + _e.cost;
                SPFA_p[_e.to] = _graph[_u][i];
                SPFA_a[_e.to] = min(SPFA_a[_u], _e.cap - _e.flow);
                if (!SPFA_inq[_e.to])
                {
                    SPFA_inq[_e.to] ++;
                    nodeQueue.push(_e.to);
                }
            }
        }
    }
    if (SPFA_d[_dst] == INF)
    {
        return false;
    }

    _flow += SPFA_a[_dst];
    _cost += SPFA_d[_dst] * SPFA_a[_dst];

    int _u = _dst;
    while (_u != _src)
    {
        _edges[SPFA_p[_u]].flow += SPFA_a[_dst];
        _edges[SPFA_p[_u] ^ 1].flow -= SPFA_a[_dst];
        _u = _edges[SPFA_p[_u]].from;
    }
    return true;
}



int getMinSolution(int* _srcNodes, int _numSources){

    //虚拟设置的单源点与单汇点
    int _src = numNodes;
    int _dst = numNodes + 1;

    //单幅图里有向边的数目、节点的数目
    int _numDirectLinks = numLinks * 2 * 2+ _numSources * 2 + numClients * 2;
    int _numNodes = numNodes + 2;

    //构建图
    int (*_graph)[MAX_LINK_PER_NODE];
    _graph = (int (*)[MAX_LINK_PER_NODE]) malloc(sizeof(int) * _numNodes * MAX_LINK_PER_NODE);
    memset(_graph, 0, sizeof(int) * _numNodes * MAX_LINK_PER_NODE);
    memcpy(_graph, graph, sizeof(int) * (_numNodes - 2) * MAX_LINK_PER_NODE);

    Edge* _edges = (Edge* )malloc(_numDirectLinks * sizeof(Edge));
    memcpy(_edges, edges, numLinks * sizeof(Edge) * 2 * 2);

    int i = numLinks * 2 * 2;
    for (int j = 0; j < _numSources; j ++)
    {
        _edges[i] = Edge(_src, _srcNodes[j], INF, 0, 0);
        _edges[i + 1] = Edge(_srcNodes[j], _src, 0, 0,0);

        _graph[_src][++_graph[_src][0]] = i;
        _graph[_srcNodes[j]][++_graph[_srcNodes[j]][0]] = i + 1;

        i += 2;
    }


    for(int j = 0; j < numClients; j++){
        _edges[i] = Edge(clients[j], _dst, clientReqs[j], 0, 0);
        _edges[i + 1] = Edge(_dst, clients[j], 0, 0, 0);

        _graph[clients[j]][++_graph[clients[j]][0]] = i;
        _graph[_dst][++_graph[_dst][0]] = i + 1;

        i += 2;
    }

    /*********************************网络图构建完毕***********************************/

    //查找最小费用最大流
    int _flow = 0;
    int _cost = 0;
    while(SPFA(_src, _dst, _graph, _edges, _numNodes, _numDirectLinks, _flow, _cost)){}

    //printf("min cost:%d\n", _cost);
    //printf("max flow:%d\n", _flow);
    /****************************查找到最小费用最大流*********************************/

    //检查是否满足流量需求
    if (_flow < totalReqs)
    {
        free(_graph);
        free(_edges);
        return -1;
    }

    int _totalCost = _cost + _numSources * costOfVideoNode;

    //更新最优解
    if (_totalCost < best_totalCost) {
        best_totalCost = _totalCost;
        free(best_srcs);
        best_srcs = (int* )malloc(sizeof(int) * _numSources);
        memcpy(best_srcs, _srcNodes, sizeof(int) * _numSources);
        best_numSrcs = _numSources;
        memcpy(best_edges, _edges, sizeof(Edge) * numLinks * 2 * 2);
    }

    //释放局部分配的图结构
    free(_graph);
    free(_edges);


    return _totalCost;


}

typedef int (*GRAPH)[MAX_LINK_PER_NODE];
int findPaths(int (*&paths)[MAX_NODE_PER_LINK]) {
    int vSrc = numNodes;
    int vDst = numNodes + 1;

    int nDirectLinks = numLinks * 2 * 2 + best_numSrcs + numClients;
    int nTotalNodes = numNodes + 2;

    GRAPH newGraph = (GRAPH) malloc(sizeof(int) * nTotalNodes * MAX_LINK_PER_NODE);
    memset(newGraph, 0, sizeof(int) * nTotalNodes * MAX_LINK_PER_NODE);
    memcpy(newGraph, graph, sizeof(int) * (nTotalNodes - 2) * MAX_LINK_PER_NODE);

    Edge* newEdges = (Edge*) malloc(nDirectLinks * sizeof(Edge));
    memcpy(newEdges, best_edges, sizeof(Edge) * numLinks * 2 * 2);

    int edgeIdx = numLinks * 2 * 2;

    // client edges must be added first to make output flow available
    for (int clientIdx = 0; clientIdx < numClients; clientIdx++) {
        newEdges[edgeIdx] = Edge(clients[clientIdx], vDst, INF, 0, clientReqs[clientIdx]);
        newGraph[clients[clientIdx]][++newGraph[clients[clientIdx]][NUM_EDGES]] = edgeIdx;
        edgeIdx++;
    }

    for (int serverIdx = 0; serverIdx < best_numSrcs; serverIdx++) {

        // restrict flows from vSrc to all servers
        int bandwidth = 0;
        for (int nbrIdx = 1; nbrIdx <= newGraph[best_srcs[serverIdx]][NUM_EDGES]; nbrIdx++) {
            bandwidth += newEdges[newGraph[best_srcs[serverIdx]][nbrIdx]].flow;
        }

        newEdges[edgeIdx] = Edge(vSrc, best_srcs[serverIdx], INF, 0, bandwidth);
        newGraph[vSrc][++newGraph[vSrc][NUM_EDGES]] = edgeIdx;
        edgeIdx++;
    }

    int pathIdx = 0;

    int prevNodeNum[MAX_NODE_PER_LINK];
    int prevEdgeNum[MAX_NODE_PER_LINK];
    int augment[MAX_NODE_PER_LINK];

    while (true) {

        prevNodeNum[vSrc] = -1;
        prevEdgeNum[vSrc] = -1;
        augment[vSrc] = INF;

        int node = vSrc;
        int nNodesInPath = 0;

        bool noPathFound = false;
        while (node != vDst) {

            bool found = false;
            for (int nbrIdx = 1; nbrIdx <= newGraph[node][NUM_EDGES]; nbrIdx++) {
                int edgeNum = newGraph[node][nbrIdx];
                Edge& e = newEdges[edgeNum];

                if (e.flow > 0) {
                    prevNodeNum[e.to] = node;
                    prevEdgeNum[e.to] = edgeNum;
                    augment[e.to] = min(augment[node], e.flow);
                    node = e.to;
                    nNodesInPath++;

                    found = true;
                    break;
                }
            }
            if (!found) {
                noPathFound = true;
                break;
            }
        }

        if (noPathFound)
            break;

        if (augment[vDst] > 0) {
            // save number of nodes (including the client) at the beginning of the path
            paths[pathIdx][NUM_NODES] = nNodesInPath;
            // save the allocated bandwidth at the end of the path
            paths[pathIdx][nNodesInPath + 1] = augment[vDst];

            int prevNode = prevNodeNum[vDst];
            int prevEdge = prevEdgeNum[prevNode];

            // get the client from map
            paths[pathIdx][nNodesInPath] = client_consumer_map[prevNode];

            for (int nodeIdx = paths[pathIdx][NUM_NODES] - 1; nodeIdx >= 1; nodeIdx--) {
                // recover the path from end to beginning
                paths[pathIdx][nodeIdx] = prevNode;
                // remove allocated flow
                newEdges[prevEdge].flow -= augment[vDst];

                prevNode = prevNodeNum[prevNode];
                prevEdge = prevEdgeNum[prevNode];
            }

            pathIdx++;
        }

    }

    //释放函数中分配的空间
    free(newEdges);
    free(newGraph);
    return pathIdx;
}

/****************************工具函数*********************************/

//返回一个节点的度
int getDegree(int node){
    return graph[node][0] / 2;
}

//交换两个数
void swap(int &a, int &b){
    int t = a;
    a = b;
    b = t;
}

//从长度n的数组a中随机抽出m个数
int* pickRandom(int* a, int n, int m){
    int* b = (int* )malloc(sizeof(int)*m);
    for(int i=0; i<m; ++i){
        int j = rand() % (n-i) + i;// 产生i到n-1间的随机数
        swap(a[i], a[j]);
    }

    for (int i=0; i<m; ++i){
        b[i] = a[i];
    }
    return b;
}

//随机返回[a, b)的一个整数
int getRandom(int a, int b){
    return (rand() % (b-a))+ a;
}

//对节点数组进行消费需求排序
//传入消费节点构成的数组和个数，返回一个数组，按照其消费需求排好序
int* sortRequest(int* currentClients, int numCurrentClients){

    //构建节点排序
    int numSortedClients = numCurrentClients;
    Node* sortedClients = (Node*)malloc(sizeof(Node) * numSortedClients);
    memset(sortedClients, 0, sizeof(Node) * numSortedClients);

    //遍历图
    for (int i = 0; i < numSortedClients; i += 1) {
        sortedClients[i].index = currentClients[i];
        int* temp = find(clients, clients + numClients, currentClients[i]);
        long index = distance(clients, temp);
        sortedClients[i].request = clientReqs[index];
    }

    //排序
    sort(sortedClients, sortedClients + numSortedClients);

    int* sorted = (int*)malloc(sizeof(int)*numSortedClients);
    for (int i = 0; i < numSortedClients; i++){
        sorted[i] = sortedClients[i].index;
    }
    free(sortedClients);

    return sorted;
}

//在一个解中贪心减少节点
int* reduceServers(int* &_sortedClients, int &_initServersNum, vector<int> &protect){

    int* solution = (int* )malloc(sizeof(int) * _initServersNum);
    memcpy(solution, _sortedClients, sizeof(int) * _initServersNum);
    //free(_sortedClients);
    int serversNum = _initServersNum;
    int lastIndex = _initServersNum - 1;
    bool isInProtect;
    while(serversNum > 0 && lastIndex >= 0) {
        serversNum--;
        int *_sol = (int *) malloc(sizeof(int) * serversNum);
        memcpy(_sol, solution, sizeof(int) * lastIndex);
        memcpy(_sol + lastIndex, solution + lastIndex + 1, sizeof(int) * (serversNum - lastIndex));
        /****/
        isInProtect = false;
        if(protect.size() != 0 && find(protect.begin(), protect.end(), solution[lastIndex]) != protect.end()){
            isInProtect = true;
        }
        /****/
        if (getMinSolution(_sol, serversNum) < 0 || isInProtect) {
            lastIndex--;
            serversNum++;
            free(_sol);
        } else {
            free(solution);
            solution = _sol;
            lastIndex--;
        }

        if (getMs() - start_time > MAX_RUNTIME) {
            _initServersNum = serversNum;
            return solution;
        }
        //printf("Server Num: %d\n", serversNum);
    }

    _initServersNum = serversNum;
    return solution;

}

/****************************找解的邻域*********************************/

void findNextDeploy(ServerDeploy* &_currentDeploy, ServerDeploy* &_newDeploy){

    int serverNum = _currentDeploy->num;

    int* servers_swap = (int*)malloc(sizeof(int)*serverNum);
    int* servers_add = (int*)malloc(sizeof(int)*(serverNum+1));

    memcpy(servers_swap, _currentDeploy->servers, sizeof(int)*serverNum);

    //与外面一个节点交换，in是servers下标，out是节点标号
    int in = getRandom(0, serverNum);
    int out = servers_swap[in];
    //不能与servers中的重复
    if (serverNum < numClients) {
        while (1) {
            out = clients[getRandom(0, numClients)];
            bool isUnique = true;
            for (int i = 0; i < serverNum; i++) {
                if (out == servers_swap[i]) {
                    isUnique = false;
                }
            }
            if (isUnique) {
                break;
            }
        }
    }
    servers_swap[in] = out;


    //三种情况分别调用MCMF
    int swapCost = getMinSolution(servers_swap, serverNum);
    int addCost = -1;

    int reduceCost;
    //减少 REDUCE_NUM 个节点

    int *temp = (int *)malloc(sizeof(int) * serverNum);
    memcpy(temp, servers_swap, sizeof(int) * serverNum);
    int* sortedServers_reduce = sortRequest(temp, serverNum);

    reduceCost = getMinSolution(sortedServers_reduce, serverNum - reduceNum);
    free(temp);

    //三种方案都没找到满足的最大流
    if (swapCost<0 && addCost<0 && reduceCost<0){
        _newDeploy->cost = -1;
        _newDeploy->num = INF;
        free(servers_swap);
        free(servers_add);
        free(sortedServers_reduce);
        return;
    }


    if (swapCost<0){
        swapCost = INF;
    }
    if (addCost<0){
        addCost = INF;
    }
    if (reduceCost<0){
        reduceCost = INF;
    }

    //构造返回值
    if (swapCost < min(addCost, reduceCost)){
        _newDeploy->servers = servers_swap;
        free(servers_add);
        free(sortedServers_reduce);
        _newDeploy->num = serverNum;
        _newDeploy->cost = swapCost;
    }
    else if (addCost < min(swapCost, reduceCost)){
        _newDeploy->servers = servers_add;
        free(servers_swap);
        free(sortedServers_reduce);
        _newDeploy->num = serverNum+1;
        _newDeploy->cost = addCost;
    }
    else if (reduceCost < min(addCost, swapCost)){
        _newDeploy->servers = sortedServers_reduce;
        free(servers_add);
        free(servers_swap);
        _newDeploy->num = serverNum - reduceNum;
        _newDeploy->cost = reduceCost;

    }

}


/****************************退火循环找server部署方案*********************************/

void findServerDeployForSmall(){

    //初始温度
    //TODO 改进：T = (F_max - F_min) / ln(P0)
    double T = T0;

    //初始放置server个数
    int initNumServers = numClients;
    //初始放置方案
    int* initServers = (int*)malloc(sizeof(int)*initNumServers);
    memcpy(initServers, clients, sizeof(int) * numClients);
    //初始开销
    int initCost = costOfVideoNode * initNumServers;

    //指向当前解
    ServerDeploy* currentDeploy = (ServerDeploy*)malloc(sizeof(ServerDeploy));
    currentDeploy->servers = initServers;
    currentDeploy->num = initNumServers;
    currentDeploy->cost = initCost;
    //指向新解
    ServerDeploy* newDeploy;

    int preServersNum = initNumServers;

    //降温计时器
    int time = 0;
    //主循环，每次循环以温度 T 搜索 INTRA_ITER_NUM 遍
    while (T > T_MIN){
        //printf("Time Count: %d\n", time);
        time += 1;

        for (int i = 0; i < INTRA_ITER_NUM; i++){

            //找新解
            newDeploy = (ServerDeploy*)malloc(sizeof(ServerDeploy));
            findNextDeploy(currentDeploy, newDeploy);
            //printf("ServerNum: %d\n", newDeploy->num);


            //动态变化reduceNum
            if (preServersNum - newDeploy->num <= 0){
                reduceUnchange++;
            }
            if (reduceUnchange >= MAX_REDUCE_UNCHANGE){
                if (reduceNum > 1) {
                    reduceNum--;
                }
                reduceUnchange = 0;
            }
            if (preServersNum - newDeploy->num > 0){
                reduceUnchange = 0;
                if (reduceNum < MAX_REDUCE_NUM) {
                    reduceNum++;
                }
            }
            preServersNum = newDeploy->num;


            //没有找到可行解，重新循环
            if (newDeploy->cost < 0){
                free(newDeploy);
                continue;
            }

            int delta = newDeploy->cost - currentDeploy->cost;
            double P = exp((-1 * delta)/T);
            //如果新解有改进 or 没改进但满足Metropolis准则
            if (delta < 0  ||  rand()/double(RAND_MAX) < P){
                //接受新解
                free(currentDeploy->servers);
                free(currentDeploy);
                currentDeploy = newDeploy;
            }
            else{
                //不接受新解，释放空间
                free(newDeploy->servers);
                free(newDeploy);
            }

            //判断运行时间，并退出
            //printf("run time: %ld\n", getMs() - start_time);
            if(getMs() - start_time > MAX_RUNTIME) {
                //printf("************* Time Run out **************\n");
                //输出最后的开销
                //printf("Total Cost: %d\n", best_totalCost);
                //printf("Servers Number: %d\n", best_numSrcs);
                return;
            }

        }

        //快速降温曲线
        T = T0 / (1 + time);
        //经典降温曲线
        //T /= log10(1 + time);
    }

    //输出最后的开销
    //printf("Total Cost: %d\n", best_totalCost);
    //printf("Servers Number: %d\n", best_numSrcs);

}


//部署节点
void findServersDeployForLarge(){

    int* sortedClients = sortRequest(clients, numClients);

    //int initServersNum = (int)(numClients * 0.2);
    int initServersNum = 1;

    //每次贪心增加一个节点，直到出现可行解
    while(getMinSolution(sortedClients, initServersNum) < 0){
        //printf("ServersNum: %d\n", initServersNum);
        initServersNum ++;
        if (getMs() - start_time > MAX_RUNTIME - 100)
        {
            //printf("Best Cost: %d\n", best_totalCost);
            break;
        }
    }
    vector<int> protect;
    //在可行解基础上倒序减少，无解则不删减跳过
    int* greedySolution = reduceServers(sortedClients, initServersNum, protect);
    free(sortedClients);

    //printf("Best Cost: %d\n", best_totalCost);

    /***进行随机扰动***/
    //随机增加一个未在解里的节点，再进行一轮消减
    int numUnpicked = numClients - initServersNum;
    do {
        //取出未选择的点
        int* setUnpicked = (int*)malloc(sizeof(int)*numUnpicked);

        //构造未取节点数组
        int index = 0;
        for (int i = 0; i < numClients; i++){
            if (find(greedySolution, greedySolution + initServersNum, clients[i]) == greedySolution + initServersNum){
                //未找到
                setUnpicked[index] = clients[i];
                index++;
            }
        }

        //加入一个随机未选择节点
        int addIndex = getRandom(0, numUnpicked);
        int add = setUnpicked[addIndex];
        free(setUnpicked);
        protect.push_back(add);
        //printf("Add Server: %d\n", add);
        int *addSolution = (int *) malloc(sizeof(int) * (initServersNum + 1));
        memcpy(addSolution, greedySolution, sizeof(int) * (initServersNum));
        addSolution[initServersNum] = add;
        //排序
        int addNum = initServersNum + 1;
        int* temp = sortRequest(addSolution, addNum);
        free(addSolution);
        addSolution = temp;
        //进行一轮消减
        int *shuffleSolution = reduceServers(addSolution, addNum, protect);
        free(addSolution);
        free(greedySolution);
        greedySolution = shuffleSolution;
        initServersNum = addNum;
        numUnpicked = numClients - addNum;
        //printf("reduced Num: %d\n", addNum);

        if (getMs() - start_time > MAX_RUNTIME) {
            //printf("Best Cost: %d\n", best_totalCost);
            return;
        }
    } while(numUnpicked > 0);

    //printf("Best Cost: %d\n", best_totalCost);
    return;

}

//你要完成的功能总入口
void deploy_server(char * topo[MAX_EDGE_NUM], int line_num,char * filename)
{
    //改种子
    srand((unsigned int)time(NULL));

    //记录开始时间
    start_time = getMs();

    //初始化数据
    initData(topo, line_num);

    //找服务器放置位置和流量分配方案
    if (numNodes <= 500){
        findServerDeployForSmall();
    }else{
        findServersDeployForLarge();
    }


    //从全局最优解中找路径
    int (*paths)[MAX_NODE_PER_LINK] = (int (*)[MAX_NODE_PER_LINK]) malloc(sizeof(int) * MAX_NODE_PER_LINK * MAX_PATH_NUM);
    int pathNum = findPaths(paths);

    char* topo_file = (char* )malloc(sizeof(char) * MAX_OUTPUT_STRING_LENGTH);
    memset(topo_file, '\0', sizeof(char) * MAX_OUTPUT_STRING_LENGTH);

    int output_len = sprintf(topo_file, "%d\n\n", pathNum);

    for (int i = 0; i < pathNum; i += 1) {
        int j = 1;
        for (; j <= paths[i][NUM_NODES]; j++) {
            output_len += sprintf(topo_file + output_len, "%d ", paths[i][j]);
        }
        output_len += sprintf(topo_file + output_len, "%d\n", paths[i][j]);

    }
    free(paths);

    // 直接调用输出文件的方法输出到指定文件中(ps请注意格式的正确性，如果有解，第一行只有一个数据；第二行为空；
    // 第三行开始才是具体的数据，数据之间用一个空格分隔开)
    write_result(topo_file, filename);

}