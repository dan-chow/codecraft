int ZKW_DST;
int ZKW_COST;
int ZKW_ANSWER;
int ZKW_NUM_NODES;
bool * ZKW_VISITED;

bool modifyLabel(int (*newGraph)[MAX_LINK_PER_NODE], Edge* newEdges) {
	int delta = INF;

	for (int nodeIdx = 0; nodeIdx < ZKW_NUM_NODES; nodeIdx++) {
		printf("node:%d\n", nodeIdx);
		if (ZKW_VISITED[nodeIdx]) {
			for (int nbrIdx = 1; nbrIdx <= newGraph[nodeIdx][NUM_EDGES]; nbrIdx++) {
				printf("edge:%d\n", newGraph[nodeIdx][nbrIdx]);
				Edge &e = newEdges[newGraph[nodeIdx][nbrIdx]];
				if (e.cap > 0 && !ZKW_VISITED[e.to] && e.cost < delta) {
					delta = e.cost;
				}
			}
		}
	}
	if (delta == INF)
		return false;

	for (int nodeIdx = 0; nodeIdx < ZKW_NUM_NODES; nodeIdx++) {
		if (ZKW_VISITED[nodeIdx]) {
			for (int nbrIdx = 1; nbrIdx <= newGraph[nodeIdx][NUM_EDGES]; nbrIdx++) {
				int edgeNum = newGraph[nodeIdx][nbrIdx];
				Edge& edgeForward = newEdges[edgeNum];
				Edge& edgeBackward = newEdges[edgeNum ^ 1];
				edgeForward.cost -= delta;
				edgeBackward.cost += delta;
			}
		}
	}

	ZKW_COST += delta;
	return true;
}

int findAugment(int node, int flow, int (*newGraph)[MAX_LINK_PER_NODE], Edge* newEdges) {
	if (node == ZKW_DST) {
		ZKW_ANSWER += ZKW_COST * flow;
		return flow;
	}

	ZKW_VISITED[node] = true;
	int flowLeft = flow;

	for (int nbrIdx = 1; nbrIdx <= graph[node][NUM_EDGES] && flowLeft > 0; nbrIdx++) {
		int edgeNum = newGraph[node][nbrIdx];
		Edge & edgeForward = newEdges[edgeNum];
		Edge & edgeBackward = newEdges[edgeNum ^ 1];

		if (edgeForward.cap > 0 && edgeForward.cost > 0 && !ZKW_VISITED[edgeForward.to]) {
			int delta = findAugment(edgeForward.to, min(flowLeft, edgeForward.cap), newGraph, newEdges);
			edgeForward.cap -= delta;
			edgeBackward.cap += delta;
			flowLeft -= delta;
		}
	}
	return flow - flowLeft;
}

int getMinSolution(int* _srcNodes, int _numSources) {

	//虚拟设置的单源点与单汇点
	int _src = numNodes;
	int _dst = numNodes + 1;

	//单幅图里有向边的数目、节点的数目
	int _numDirectLinks = numLinks * 2 * 2 + _numSources * 2 + numClients * 2;
	int _numNodes = numNodes + 2;

	//构建图
	int (*_graph)[MAX_LINK_PER_NODE];
	_graph = (int (*)[MAX_LINK_PER_NODE]) malloc(sizeof(int) * _numNodes * MAX_LINK_PER_NODE);
	memset(_graph, 0, sizeof(int) * _numNodes * MAX_LINK_PER_NODE);
	memcpy(_graph, graph, sizeof(int) * (_numNodes - 2) * MAX_LINK_PER_NODE);

	Edge* _edges = (Edge*) malloc(_numDirectLinks * sizeof(Edge));
	memcpy(_edges, edges, numLinks * sizeof(Edge) * 2 * 2);

	int i = numLinks * 2 * 2;
	for (int j = 0; j < _numSources; j++) {
		_edges[i] = Edge(_src, _srcNodes[j], INF, 1, 0);
		_edges[i + 1] = Edge(_srcNodes[j], _src, 0, -1, 0);

		_graph[_src][++_graph[_src][0]] = i;
		_graph[_srcNodes[j]][++_graph[_srcNodes[j]][0]] = i + 1;

		i += 2;
	}

	for (int j = 0; j < numClients; j++) {
		_edges[i] = Edge(clients[j], _dst, clientReqs[j], 1, 0);
		_edges[i + 1] = Edge(_dst, clients[j], 0, -1, 0);

		_graph[clients[j]][++_graph[clients[j]][0]] = i;
		_graph[_dst][++_graph[_dst][0]] = i + 1;

		i += 2;
	}

	/*********************************网络图构建完毕***********************************/

//	int ZKW_DST;
//	int ZKW_COST;
//	int ZKW_ANSWER;
//	int ZKW_NUM_NODES;
//	bool * ZKW_VISITED;

	ZKW_DST = _dst;
	ZKW_COST = ZKW_ANSWER = 0;
	ZKW_NUM_NODES = _numNodes;
	ZKW_VISITED = (bool*) malloc(sizeof(bool) * _numNodes);

	do {
		//printf("outer\n");
		do {
			//printf("inner\n");
			memset(ZKW_VISITED, false, sizeof(bool) * _numNodes);
		} while (findAugment(_src, INF, _graph, _edges));
	} while (modifyLabel(_graph, _edges));
	
	/****************************查找到最小费用最大流*********************************/

	free(ZKW_VISITED);

	return ZKW_ANSWER;
}
